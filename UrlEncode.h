//
//  UrlEncode.h
//  HomeLinerNow
//
//  Created by foobar on 2013/05/19.
//
//

#import <Foundation/Foundation.h>

@interface UrlEncode : NSObject

+ (NSString *)encode:(NSString *)original;

@end
