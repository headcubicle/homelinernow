//
//  HomeLinerNowValues.m
//  HomeLinerNow
//
//  Created by foobar on 2013/02/17.
//
//

#import "HomeLinerNowValues.h"

@implementation HomeLinerNowValues

static HomeLinerNowValues* homeLinerNowValues = nil;

+ (HomeLinerNowValues *)sharedManager
{
    @synchronized(self)
    {
        if (homeLinerNowValues == nil)
        {
            [[self alloc] init];
        }
    }
    
    return homeLinerNowValues;
}

// インスタンス生成
+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self)
    {
        if (homeLinerNowValues == nil)
        {
            homeLinerNowValues = [super allocWithZone:zone];
            
            // 固定値初期化
            [homeLinerNowValues initValues];
            
            return homeLinerNowValues;
        }
    }
    
    return nil;
}

- (void)initValues
{
    // 固定値初期化
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *valuesPath = [bundle pathForResource:@"Values" ofType:@"plist"];
    NSDictionary *values = [NSDictionary dictionaryWithContentsOfFile:valuesPath];
    _twitterApiUrl = [values valueForKey:@"twitterApiUrl"];
    _twitterApiParameterKey = [values valueForKey:@"twitterApiParameterKey"];
    _distanceFromStation = [values valueForKey:@"distanceFromStation"];
    _secondsOfAday = [values valueForKey:@"secondsOfAday"];
    _timeRangeOfAboardFrom = [values valueForKey:@"timeRangeOfAboardFrom"];
    _timeRangeOfAboardTo = [values valueForKey:@"timeRangeOfAboardTo"];
    _timeRangeOfMissTo = [values valueForKey:@"timeRangeOfMissTo"];
    _timeRangeOfWaitFrom = [values valueForKey:@"timeRangeOfWaitFrom"];
    _boardingTime = [values valueForKey:@"boardingTime"];
    _radiusOfTheEarth = [values valueForKey:@"radiusOfTheEarth"];
    _timeIntervalOfLocationChangeMonitor = [values valueForKey:@"timeIntervalOfLocationChangeMonitor"];
    _offsetOfJst = [values valueForKey:@"offsetOfJst"];
    _historyFileName = [values valueForKey:@"historyFileName"];    
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

@end
