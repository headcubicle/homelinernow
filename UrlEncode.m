//
//  UrlEncode.m
//  HomeLinerNow
//
//  Created by foobar on 2013/05/19.
//
//

#import "UrlEncode.h"

@implementation UrlEncode

// URLエンコード
+ (NSString *)encode:(NSString *)original
{
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)original, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingShiftJIS));
}

@end
