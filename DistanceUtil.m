//
//  DistanceUtil.m
//  HomeLinerNow
//
//  Created by foobar on 2013/02/17.
//
//

#import "DistanceUtil.h"

@implementation DistanceUtil

static HomeLinerNowValues *homeLinerNowValues = nil;

// 距離計算
+ (double)getDistance:(double)latitudeFrom longitudeFrom:(double)longitudeFrom latitudeTo:(double)latitudeTo longitudeTo:(double)longitudeTo
{
    // 固定値取得
    homeLinerNowValues = [HomeLinerNowValues sharedManager];
   
    double fromX = [self radian:longitudeFrom];
    double fromY = [self radian:latitudeFrom];
    double toX = [self radian:longitudeTo];
    double toY = [self radian:latitudeTo];
    
    double deg = sin(fromY) * sin(toY) + cos(fromY) * cos(toY) * cos(toX - fromX);
    double distance = homeLinerNowValues.radiusOfTheEarth.doubleValue * (atan(-deg / sqrt(-deg * deg + 1.0)) + M_PI / 2.0);
    
    return distance;
}

// 度をラジアンに変換
+ (double)radian:(double)degrees
{
    return degrees * M_PI / 180;
}

@end
