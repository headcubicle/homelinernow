//
//  HomeLinerNowValues.h
//  HomeLinerNow
//
//  Created by foobar on 2013/02/17.
//
//

#import <Foundation/Foundation.h>

@interface HomeLinerNowValues : NSObject

+ (HomeLinerNowValues *)sharedManager;
+ (id)allocWithZone:(NSZone *)zone;
- (id)copyWithZone:(NSZone *)zone;

@property (copy, readonly) NSString *twitterApiUrl;
@property (copy, readonly) NSString *twitterApiParameterKey;
@property (copy, readonly) NSNumber *distanceFromStation;
@property (copy, readonly) NSNumber *secondsOfAday;
@property (copy, readonly) NSNumber *timeRangeOfAboardFrom;
@property (copy, readonly) NSNumber *timeRangeOfAboardTo;
@property (copy, readonly) NSNumber *timeRangeOfMissTo;
@property (copy, readonly) NSNumber *timeRangeOfWaitFrom;
@property (copy, readonly) NSNumber *boardingTime;
@property (copy, readonly) NSNumber *radiusOfTheEarth;
@property (copy, readonly) NSNumber *timeIntervalOfLocationChangeMonitor;
@property (copy, readonly) NSNumber *offsetOfJst;
@property (copy, readonly) NSString *historyFileName;

@end
