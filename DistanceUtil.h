//
//  DistanceUtil.h
//  HomeLinerNow
//
//  Created by foobar on 2013/02/17.
//
//

#import <Foundation/Foundation.h>
#import "HomeLinerNowValues.h"

@interface DistanceUtil : NSObject

+ (double)getDistance:(double)latitudeFrom longitudeFrom:(double)longitudeFrom latitudeTo:(double)latitudeTo longitudeTo:(double)longitudeTo;
+ (double)radian:(double)degrees;

@end
