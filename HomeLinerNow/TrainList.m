//
//  TrainList.m
//  HomeLinerNow
//
//  Created by foobar on 2012/12/31.
//
//

#import "TrainList.h"

@implementation TrainList

// 初期化
- (id)init:(NSDictionary *)trains
{
    self = [super init];
    
    if (self)
    {
        _trains = [[NSMutableDictionary alloc] init];
        
        NSArray *trainNames = trains.allKeys;
        for (NSString *trainName in trainNames)
        {
            NSLog(@"%@", trainName);
            // 駅の緯度、経度、誤差を取得
            NSDictionary *train = [trains valueForKey:trainName];
            TrainInfo *trainInfo = [[TrainInfo alloc] init:trainName trainNumber:[train valueForKey:@"trainNumber"] direction:(BOOL)[train valueForKey:@"direction"] ekinetAvailable:(BOOL)[[train valueForKey:@"ekinetAvailable"] boolValue]  timeTable:[train valueForKey:@"timeTable"]];
            [_trains setObject:trainInfo forKey:trainName];
        }
        
        return self;
    }
    
    return nil;
}

// 全列車の名前を取得
- (NSArray *)allTrainNames
{
    return _trains.allKeys;
}

// 列車の情報を取得
- (TrainInfo *)trainInfoFromKey:(NSString *)key
{
    return [_trains valueForKey:key];
}

@end
