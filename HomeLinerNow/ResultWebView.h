//
//  ResultWebView.h
//  HomeLinerNow
//
//  Created by foobar on 2013/05/26.
//
//

#import <UIKit/UIKit.h>

@interface ResultWebView : UIWebView

- (void)loadHTMLString:(NSString *)string;

@end
