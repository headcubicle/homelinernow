//
//  StationInfo.h
//  HomeLinerNow
//
//  Created by foobar on 2012/12/02.
//
//

#import <Foundation/Foundation.h>

@interface StationInfo : NSObject

@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSNumber *latitude;
@property (copy, nonatomic) NSNumber *longitude;
@property (copy, nonatomic) NSNumber *errorRange;
@property (copy, nonatomic) NSString *stationId;

- (id)init:(NSString *)name latitude:(NSNumber *)latitude longitude:(NSNumber *)longitude errorRange:(NSNumber *)errorRange stationId:(NSString *)stationId;

@end
