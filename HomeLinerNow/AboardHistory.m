//
//  AboardHistory.m
//  HomeLinerNow
//
//  Created by foobar on 2013/01/05.
//
//

#import "AboardHistory.h"

@implementation AboardHistory

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    
    if (self)
    {
        _trainName = [decoder decodeObjectForKey:@"trainName"];
        _aboardTime = [decoder decodeObjectForKey:@"aboardTime"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:_trainName forKey:@"trainName"];
    [encoder encodeObject:_aboardTime forKey:@"aboardTime"];
}

@end
