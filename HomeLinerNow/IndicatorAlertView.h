//
//  IndicatorAlartView.h
//  HomeLinerNow
//
//  Created by foobar on 2013/05/24.
//
//

#import <UIKit/UIKit.h>

/**
 * UIAlertView を拡張したコントロールです。
 */
@interface IndicatorAlertView : UIAlertView<UIAlertViewDelegate>

+ (IndicatorAlertView *)alertWithButton:(NSString *)title message:(NSString *)message delegate:(id)delegate button:(NSString *)button;
+ (IndicatorAlertView *)alertWithIndicator:(NSString *)title message:(NSString *)message delegate:(id)delegate button:(NSString *)button;

- (void)addButtonWithTitle:(NSString *)title selector:(SEL)selector userInfo:(id)userInfo;
- (void)close:(NSInteger)buttonIndex;
- (void)setFirstButtonDelegate:(SEL)selector userInfo:(id)userInfo;

@end