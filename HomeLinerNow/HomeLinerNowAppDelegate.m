//
//  HomeLinerNowAppDelegate.m
//  HomeLinerNow
//
//  Created by foobar on 2012/12/02.
//
//

#import "HomeLinerNowAppDelegate.h"
#import "HomeLinerNowViewController.h"

@implementation HomeLinerNowAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // 測位停止
    UINavigationController* navigationController = (UINavigationController*)self.window.rootViewController;
    [(HomeLinerNowViewController*)[[navigationController viewControllers] objectAtIndex:0] onPauseLocationManager];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // 測位再開
    UINavigationController* navigationController = (UINavigationController*)self.window.rootViewController;
    [(HomeLinerNowViewController*)[[navigationController viewControllers] objectAtIndex:0] onResumeLocationManager];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
