//
//  TimeUtil.h
//  HomeLinerNow
//
//  Created by foobar on 2013/02/17.
//
//

#import <Foundation/Foundation.h>
#import "HomeLinerNowValues.h"

@interface TimeUtil : NSObject

+ (NSTimeInterval)departingTimestamp:(NSString *)departure;
+ (NSTimeInterval)currentTimestampJst;
+ (NSString *)departingHour:(NSTimeInterval)departingAt;

@end
