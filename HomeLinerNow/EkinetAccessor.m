//
//  EkinetAccessor.m
//  HomeLinerNow
//
//  Created by foobar on 2013/03/30.
//
//

#import "EkinetAccessor.h"

@implementation EkinetAccessor

NSString *loginUrl = nil;
NSString *loginForm = nil;
NSString *viewState = nil;
NSString *reservationTop = nil;
NSString *trainSearch = nil;
NSString *trainSelect = nil;
NSString *trainNumber = nil;
NSString *trainUrl = nil;
NSString *reconfirm = nil;
NSString *reservation = nil;

BOOL isViewState = NO;
BOOL isReservationTop = NO;
BOOL isTrainSearch = NO;
BOOL isReconfirm = NO;

- (id)init
{
    self = [super init];
    
    if (self)
    {
        _ekinetUrl = @"http://www.eki-net.com/";
        _userAgent = @"Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X; ja-jp) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25";

        loginUrl = nil;
        loginForm = nil;
        viewState = nil;
        reservationTop = nil;
        trainSearch = nil;
        trainSelect = nil;
        trainNumber = nil;
        trainUrl = nil;
        reconfirm = nil;
        reservation = nil;
        
        isViewState = NO;
        isReservationTop = NO;
        isTrainSearch = NO;
        isReconfirm = NO;

        return self;
    }
    
    return nil;
}

// GETリクエスト
- (NSString *)httpGet:(NSString *)urlString function:(startElementSAXFunc)function
{
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setValue:_userAgent forHTTPHeaderField:@"User-Agent"];
    
    NSURLResponse *response;
    NSError *error;
    
    NSData *htmlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSString *htmlString = [[NSString alloc] initWithData:htmlData encoding:NSShiftJISStringEncoding];
    NSLog(@"%d", ((NSHTTPURLResponse *)response).statusCode);
    NSLog(@"%@", ((NSHTTPURLResponse *)response).allHeaderFields);
    NSLog(@"%@", htmlString);
    
    htmlParserCtxtPtr parserContext;
    htmlSAXHandler saxHandlerStruct = {0};
    saxHandlerStruct.startElement = function;
    parserContext = htmlCreatePushParserCtxt(&saxHandlerStruct, NULL, NULL, 0, NULL, XML_CHAR_ENCODING_SHIFT_JIS);
    int options = htmlCtxtUseOptions(parserContext, HTML_PARSE_RECOVER | HTML_PARSE_NOERROR | HTML_PARSE_NOWARNING);
    int result = htmlParseChunk(parserContext, (const char *)[htmlData bytes], [htmlData length], 1);
    // ここでresult == 68 (XML_ERR_NAME_REQUIRED)になる。
    htmlFreeParserCtxt(parserContext);
    
    return htmlString;
}

// POSTリクエスト
- (NSString *)httpPost:(NSString *)urlString requestBody:(NSString *)requestBody  function:(startElementSAXFunc)function
{
    NSURL *url = [NSURL URLWithString:urlString];
    NSData *requestBodyData = [requestBody dataUsingEncoding:NSShiftJISStringEncoding];
    
    NSLog(@"shift_jis: %@", requestBody);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:_userAgent forHTTPHeaderField:@"User-Agent"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [requestBodyData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestBodyData];
    [request setHTTPShouldHandleCookies:YES];
    
    NSURLResponse *response;
    NSError *error;
    
    NSData *htmlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSString *htmlString = [[NSString alloc] initWithData:htmlData encoding:NSShiftJISStringEncoding];
    NSLog(@"%d", ((NSHTTPURLResponse *)response).statusCode);
    NSLog(@"%@", ((NSHTTPURLResponse *)response).allHeaderFields);
    NSLog(@"%@", htmlString);
    
    if (function != nil) {
        htmlParserCtxtPtr parserContext;
        htmlSAXHandler saxHandlerStruct = {0};
        saxHandlerStruct.startElement = function;
        parserContext = htmlCreatePushParserCtxt(&saxHandlerStruct, NULL, NULL, 0, NULL, XML_CHAR_ENCODING_SHIFT_JIS);
        int options = htmlCtxtUseOptions(parserContext, HTML_PARSE_RECOVER | HTML_PARSE_NOERROR | HTML_PARSE_NOWARNING);
        int result = htmlParseChunk(parserContext, (const char *)[htmlData bytes], [htmlData length], 1);
        // ここでresult == 68 (XML_ERR_NAME_REQUIRED)になる。
        htmlFreeParserCtxt(parserContext);
    }
    
    return htmlString;
}

// トップ画面表示
- (NSString *)openEkinet
{
    [self httpGet:_ekinetUrl function:&loginUrlHandler];
    return loginUrl;
}

// ログイン画面表示
- (NSString *)openLogin:(NSString *)loginUrlString
{
    [self httpGet:loginUrlString function:&loginFormHandler];
    return loginForm;
}

// ログイン
- (NSString *)login:(NSString *)loginFormString
{
    NSString *requestBody = [NSString stringWithFormat:@"__VIEWSTATE=%@&TxtUserID=%@&TxtPassword=%@&BtnLogin=%@", [UrlEncode encode:viewState], [UrlEncode encode:@"speed-restriction@iijmio-mail.jp"], [UrlEncode encode:@"j2wE1d3bext"], [UrlEncode encode:@"ログイン"]];
    [self httpPost:loginFormString requestBody:requestBody function:&reservationTopHandler];
    return reservationTop;
}

// ライナー券予約トップを表示
- (NSString *)openReservationTop:(NSString *)reservationTopString
{
    [self httpGet:reservationTopString function:&trainSearchHandler];
    return trainSearch;
}

// 列車検索画面を表示
- (NSString *)openTrainSearch:(NSString *)reservationString
{
    [self httpGet:reservationString function:&trainSelectHandler];
    return trainSelect;
}

// 列車選択画面を表示
- (NSString *)openTrainSelect:(NSString *)trainSelectUrlString requsetBody:(NSString *)requestBody trainNumberString:(NSString *)trainNumberString
{
    trainNumber = trainNumberString;
    [self httpPost:trainSelectUrlString requestBody:requestBody function:&trainUrlHandler];
    return trainUrl;
}

// 確認画面を表示
- (NSString *)openConfirm:(NSString *)confirmUrlString
{
    [self httpGet:confirmUrlString function:&reservationHandler];
    if (reservation == nil) {
        [self httpGet:reconfirm function:&reservationHandler];
    }
    return reservation;
}

// 予約確定画面を表示
- (NSString *)reservation:(NSString *)reservationUrlString
{
    NSString *requestBody = [NSString stringWithFormat:@"BtnReserve=%@", [UrlEncode encode:@"9.予約する"]];
    return [self httpPost:reservationUrlString requestBody:requestBody function:nil];
}

// ログインURLを取得
static void loginUrlHandler(void *ctx, const xmlChar *name, const xmlChar **atts)
{
    NSString *nameString = [[NSString alloc] initWithUTF8String:(char *)name];
    
    // a要素を取得
    if ([nameString isEqualToString:@"a"])
    {
        if (atts != NULL)
        {
            for(int i = 0; atts[i] != NULL && loginUrl == nil; i++)
            {
                // href属性を取得
                NSString *attributeString = [[NSString alloc] initWithUTF8String:(char *)atts[i]];
                if ([attributeString isEqualToString:@"href"] && atts[i + 1] != NULL)
                {
                    NSString *urlString = [[NSString alloc] initWithUTF8String:(char *)atts[i + 1]];
                    NSRange hasLogin = [urlString rangeOfString:@"Login"];
                    // "Login"が含まれているか
                    if (hasLogin.location != NSNotFound)
                    {
                        loginUrl = urlString;
                        NSLog(@"url=%@", loginUrl);
                    }
                }
            }
        }
    }
}

// ログインフォームURLを取得
static void loginFormHandler(void *ctx, const xmlChar *name, const xmlChar **atts)
{
    NSString *nameString = [[NSString alloc] initWithUTF8String:(char *)name];
    
    // form要素を取得
    if ([nameString isEqualToString:@"form"])
    {
        if (atts != NULL)
        {
            for(int i = 0; atts[i] != NULL && loginForm == nil; i++)
            {
                // action属性を取得
                NSString *attributeString = [[NSString alloc] initWithUTF8String:(char *)atts[i]];
                if ([attributeString isEqualToString:@"action"] && atts[i + 1] != NULL)
                {
                    NSString *urlString = [[NSString alloc] initWithUTF8String:(char *)atts[i + 1]];
                    NSRange hasLogin = [urlString rangeOfString:@"Login"];
                    // "Login"が含まれているか
                    if (hasLogin.location != NSNotFound)
                    {
                        loginForm = urlString;
                        NSLog(@"url=%@", loginForm);
                    }
                }
            }
        }
    }
    // input要素を取得
    else if ([nameString isEqualToString:@"input"])
    {
        if (atts != NULL)
        {
            for(int i = 0; atts[i] != NULL && viewState == nil; i++)
            {
                NSString *attributeString = [[NSString alloc] initWithUTF8String:(char *)atts[i]];
                // name属性を取得
                if ([attributeString isEqualToString:@"name"] && atts[i + 1] != NULL)
                {
                    NSString *urlString = [[NSString alloc] initWithUTF8String:(char *)atts[i + 1]];
                    NSRange hasViewState = [urlString rangeOfString:@"__VIEWSTATE"];
                    // "__VIEWSTATE"が含まれているか
                    if (hasViewState.location != NSNotFound)
                    {
                        isViewState = YES;
                    }
                }
                // value属性を取得
                else if ([attributeString isEqualToString:@"value"] && atts[i + 1] != NULL)
                {
                    NSString *urlString = [[NSString alloc] initWithUTF8String:(char *)atts[i + 1]];
                    if (isViewState)
                    {
                        viewState = urlString;
                        NSLog(@"__VIEWSTATE=%@", viewState);
                    }
                }
            }
        }
    }
}

// 予約画面TOP URLを取得
static void reservationTopHandler(void *ctx, const xmlChar *name, const xmlChar **atts)
{
    NSString *nameString = [[NSString alloc] initWithUTF8String:(char *)name];
    
    // a要素を取得
    if ([nameString isEqualToString:@"a"])
    {
        if (atts != NULL)
        {
            for(int i = 0; atts[i] != NULL && reservationTop == nil; i++)
            {
                NSString *attributeString = [[NSString alloc] initWithUTF8String:(char *)atts[i]];
                // accesskey属性を取得
                if ([attributeString isEqualToString:@"accesskey"] && atts[i + 1] != NULL)
                {
                    NSString *accesskey = [[NSString alloc] initWithUTF8String:(char *)atts[i + 1]];
                    // ライナー券予約のリンクか
                    if ([accesskey intValue] == 2)
                    {
                        isReservationTop = YES;
                    }
                }
                // value属性を取得
                else if ([attributeString isEqualToString:@"href"] && atts[i + 1] != NULL)
                {
                    NSString *urlString = [[NSString alloc] initWithUTF8String:(char *)atts[i + 1]];
                    if (isReservationTop)
                    {
                        reservationTop = urlString;
                        NSLog(@"reservationTop=%@", reservationTop);
                    }
                }
            }
        }
    }
}

// 列車検索画面URLを取得
static void trainSearchHandler(void *ctx, const xmlChar *name, const xmlChar **atts)
{
    NSString *nameString = [[NSString alloc] initWithUTF8String:(char *)name];
    
    // a要素を取得
    if ([nameString isEqualToString:@"a"])
    {
        if (atts != NULL)
        {
            for(int i = 0; atts[i] != NULL && trainSearch == nil; i++)
            {
                NSString *attributeString = [[NSString alloc] initWithUTF8String:(char *)atts[i]];
                // accesskey属性を取得
                if ([attributeString isEqualToString:@"accesskey"] && atts[i + 1] != NULL)
                {
                    NSString *accesskey = [[NSString alloc] initWithUTF8String:(char *)atts[i + 1]];
                    // 列車検索画面のリンクか
                    if ([accesskey intValue] == 1)
                    {
                        isTrainSearch = YES;
                    }
                }
                // value属性を取得
                else if ([attributeString isEqualToString:@"href"] && atts[i + 1] != NULL)
                {
                    NSString *urlString = [[NSString alloc] initWithUTF8String:(char *)atts[i + 1]];
                    if (isTrainSearch)
                    {
                        trainSearch = urlString;
                        NSLog(@"trainSearch=%@", trainSearch);
                    }
                }
            }
        }
    }
}

// 列車選択URLを取得
static void trainSelectHandler(void *ctx, const xmlChar *name, const xmlChar **atts)
{
    NSString *nameString = [[NSString alloc] initWithUTF8String:(char *)name];
    
    // form要素を取得
    if ([nameString isEqualToString:@"form"])
    {
        if (atts != NULL)
        {
            for(int i = 0; atts[i] != NULL && trainSelect == nil; i++)
            {
                NSString *attributeString = [[NSString alloc] initWithUTF8String:(char *)atts[i]];
                // action属性を取得
                if ([attributeString isEqualToString:@"action"] && atts[i + 1] != NULL)
                {
                    trainSelect = [[NSString alloc] initWithUTF8String:(char *)atts[i + 1]];
                    NSLog(@"trainSelect=%@", trainSelect);
                }
            }
        }
    }
}

// 列車URLを取得
static void trainUrlHandler(void *ctx, const xmlChar *name, const xmlChar **atts)
{
    NSString *nameString = [[NSString alloc] initWithUTF8String:(char *)name];
    
    // a要素を取得
    if ([nameString isEqualToString:@"a"])
    {
        if (atts != NULL)
        {
            for(int i = 0; atts[i] != NULL && trainUrl == nil; i++)
            {
                // href属性を取得
                NSString *attributeString = [[NSString alloc] initWithUTF8String:(char *)atts[i]];
                if ([attributeString isEqualToString:@"href"] && atts[i + 1] != NULL)
                {
                    NSString *urlString = [[NSString alloc] initWithUTF8String:(char *)atts[i + 1]];
                    NSRange hasTrainNumber = [urlString rangeOfString:trainNumber];
                    // 列車番号が含まれているか
                    if (hasTrainNumber.location != NSNotFound)
                    {
                        trainUrl = urlString;
                        NSLog(@"url=%@", trainUrl);
                    }
                }
            }
        }
    }
}

// 予約URLを取得
static void reservationHandler(void *ctx, const xmlChar *name, const xmlChar **atts)
{
    NSString *nameString = [[NSString alloc] initWithUTF8String:(char *)name];
    
    // form要素を取得
    if ([nameString isEqualToString:@"form"])
    {
        if (atts != NULL)
        {
            for(int i = 0; atts[i] != NULL && reservation == nil; i++)
            {
                NSString *attributeString = [[NSString alloc] initWithUTF8String:(char *)atts[i]];
                // action属性を取得
                if ([attributeString isEqualToString:@"action"] && atts[i + 1] != NULL)
                {
                    reservation = [[NSString alloc] initWithUTF8String:(char *)atts[i + 1]];
                    NSLog(@"reservation=%@", reservation);
                }
            }
        }
    }
    // a要素を取得
    else if ([nameString isEqualToString:@"a"])
    {
        if (atts != NULL)
        {
            for(int i = 0; atts[i] != NULL && reconfirm == nil; i++)
            {
                NSString *attributeString = [[NSString alloc] initWithUTF8String:(char *)atts[i]];
                // accesskey属性を取得
                if ([attributeString isEqualToString:@"accesskey"] && atts[i + 1] != NULL)
                {
                    NSString *accesskey = [[NSString alloc] initWithUTF8String:(char *)atts[i + 1]];
                    // 「同意して申し込みを続ける」のリンクか
                    if ([accesskey intValue] == 9)
                    {
                        isReconfirm = YES;
                    }
                }
                // value属性を取得
                else if ([attributeString isEqualToString:@"href"] && atts[i + 1] != NULL)
                {
                    NSString *urlString = [[NSString alloc] initWithUTF8String:(char *)atts[i + 1]];
                    if (isReconfirm)
                    {
                        reconfirm = urlString;
                        NSLog(@"reconfirm=%@", reconfirm);
                    }
                }
            }
        }
    }
}

@end
