//
//  StationList.m
//  HomeLinerNow
//
//  Created by foobar on 2012/12/30.
//
//

#import "StationList.h"

@implementation StationList

// 初期化
- (id)init:(NSDictionary *)stations
{
    self = [super init];
    
    if (self) {
        _stations = [[NSMutableDictionary alloc] init];
        
        NSArray *stationNames = stations.allKeys;
        for (NSString *stationName in stationNames)
        {
            NSLog(@"%@", stationName);
            // 駅の緯度、経度、誤差を取得
            NSDictionary *station = [stations valueForKey:stationName];
            StationInfo *stationInfo = [[StationInfo alloc] init:stationName latitude:[station valueForKey:@"latitude"] longitude:[station valueForKey:@"longitude"] errorRange:[station valueForKey:@"errorRange"] stationId:[station valueForKey:@"stationId"]];
            [_stations setObject:stationInfo forKey:stationName];
        }
        
        return self;
    }
    
    return nil;
}

// 全駅の名前を取得
- (NSArray *)allStationNames
{
    return _stations.allKeys;
}

// 駅の情報を取得
- (StationInfo *)stationInfoFromKey:(NSString *)key
{
    return [_stations valueForKey:key];
}

@end
