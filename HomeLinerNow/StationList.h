//
//  StationList.h
//  HomeLinerNow
//
//  Created by foobar on 2012/12/30.
//
//

#import <Foundation/Foundation.h>
#import "StationInfo.h"

@interface StationList : NSObject

@property (copy, nonatomic) NSMutableDictionary* stations;

- (id)init:(NSDictionary *)stations;
- (NSArray *)allStationNames;
- (StationInfo *)stationInfoFromKey:(NSString *)key;

@end
