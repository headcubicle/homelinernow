//
//  ReservationInfo.m
//  HomeLinerNow
//
//  Created by foobar on 2013/05/16.
//
//

#import "ReservationInfo.h"

@implementation ReservationInfo

// 予約用POSTパラメータを取得する。
- (NSString *)httpParameters
{
    NSString *parameters = [NSString stringWithFormat:@"DdlRide=%@&DdlOff=%@&DdlPerson=%d&DdlPlant=%d&DdlWay=%d&DdlTime=%@&BtnNext=%@", _take. stationId, _off.stationId, _person, _grade, _way, [TimeUtil departingHour:[_train timeTableInfoFromKey:_take.name].departingAt],[UrlEncode encode:@"9.次へ進む"]];
    
    return parameters;
}

// 普通車
+ (NSUInteger)economy
{
    return 11;
}

// グリーン車
+ (NSUInteger)green
{
    return 21;
}

// 座席位置: 指定無し
+ (NSUInteger)notSpecified
{
    return 0;
}

// 座席位置: 窓側
+ (NSUInteger)window
{
    return 1;
}

// 座席位置: 通路側
+ (NSUInteger)aisle
{
    return 2;
}

@end
