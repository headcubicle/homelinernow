//
//  TimeTable.m
//  HomeLinerNow
//
//  Created by foobar on 2012/12/31.
//
//

#import "TimeTable.h"

@implementation TimeTable

- (id)init:(NSString *)stationName timeTableInfo:(NSDictionary *)timeTableInfo
{
    self = [super init];
    
    if (self)
    {
        // 駅名
        _stationName = stationName;
        // 発車時刻をUNIX時間へ変換
        _departure = [timeTableInfo valueForKey:@"departingAt"];
        _departingAt = [TimeUtil departingTimestamp:_departure];
        // 乗車可能か
        _available = [(NSNumber *)[timeTableInfo valueForKey:@"available"] boolValue];
        // 停車駅か
        _willStop = [(NSNumber *)[timeTableInfo valueForKey:@"willStop"] boolValue];
        
        //NSLog(@"_departingAt: %lf", _departingAt);
        //NSLog(@"_departure: %@", departure);
        //NSLog(@"_available: %d", _available);
        NSLog(@"_willStop: %d", _willStop);
        
        return self;
    }
    
    return nil;
}

@end
