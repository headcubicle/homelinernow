//
//  TimeTable.h
//  HomeLinerNow
//
//  Created by foobar on 2012/12/31.
//
//

#import <Foundation/Foundation.h>
#import "TimeUtil.h"

@interface TimeTable : NSObject

@property NSString* stationName;
@property NSString* departure;
@property NSTimeInterval departingAt;
@property BOOL available;
@property BOOL willStop;

- (id)init:(NSString *)stationName timeTableInfo:(NSDictionary *)timeTableInfo;

@end
