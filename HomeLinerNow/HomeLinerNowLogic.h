//
//  HomeLinerNowLogic.h
//  HomeLinerNow
//
//  Created by foobar on 2013/06/07.
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "StationList.h"
#import "TrainList.h"
#import "AboardHistory.h"
#import "DistanceUtil.h"

@interface HomeLinerNowLogic : NSObject

@property (copy, nonatomic) StationList *stations;
@property (copy, nonatomic) TrainList *trains;
@property (copy, nonatomic) AboardHistory *history;
@property (copy, nonatomic) NSString *aboard;
@property NSTimeInterval delay;
@property BOOL isWrite;

- (id)init;
- (NSString *)getCurrentStation:(double)latitude longitude:(double)longitude;
- (void)boardingTrainAt:(NSString*)currenntStation;
- (void)checkHistoryFile;
- (void)writeHistoryFile:(AboardHistory *)aboardHistory;

@end
