//
//  HomeLinerInfo.h
//  HomeLinerNow
//
//  Created by foobar on 2012/12/02.
//
//

#import <Foundation/Foundation.h>
#import "TimeTable.h"

@interface TrainInfo : NSObject

@property (nonatomic, copy) NSString *trainName;
@property (nonatomic, copy) NSString *trainNumber;
@property BOOL direction;
@property BOOL ekinetAvailable;
@property (nonatomic, copy) NSMutableDictionary *timeTable;

- (id)init:(NSString *)trainName trainNumber:(NSString *)trainNumber direction:(BOOL)direction ekinetAvailable:(BOOL)ekinetAvailable timeTable:(NSDictionary *)timeTable;
- (NSArray *)allStationNames;
- (TimeTable *)timeTableInfoFromKey:(NSString *)key;
+ (BOOL)inBound;
+ (BOOL)outBound;

@end
