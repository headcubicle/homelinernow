//
//  HomeLinerNowAppDelegate.h
//  HomeLinerNow
//
//  Created by foobar on 2012/12/02.
//
//

#import <UIKit/UIKit.h>

@interface HomeLinerNowAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
