//
//  TimeUtil.m
//  HomeLinerNow
//
//  Created by foobar on 2013/02/17.
//
//

#import "TimeUtil.h"

@implementation TimeUtil

static HomeLinerNowValues *homeLinerNowValues = nil;

// 発車時刻秒数を取得する。
+ (NSTimeInterval)departingTimestamp:(NSString *)departure
{
    // 固定値初期化
    homeLinerNowValues = [HomeLinerNowValues sharedManager];

    // 発車時刻をUNIX時間へ変換する。
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // 時刻の表記そのままの秒数を取得するために、わざとGMTを指定する。
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"ja_JP"];
    [dateFormatter setLocale:locale];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSDate *departingDate = [dateFormatter dateFromString:departure];
    NSTimeInterval departingTimestamp = [departingDate timeIntervalSince1970];
    //NSLog(@"departingTimestamp: %Lf", fmodl(departingTimestamp, homeLinerNowValues.secondsOfAday.doubleValue));
    
    return fmodl(departingTimestamp, homeLinerNowValues.secondsOfAday.doubleValue);
}

// 現在日時の秒数をJSTで取得する。
+ (NSTimeInterval)currentTimestampJst
{
    // 固定値初期化
    homeLinerNowValues = [HomeLinerNowValues sharedManager];

    return [[NSDate date] timeIntervalSince1970] + homeLinerNowValues.offsetOfJst.doubleValue;
}

// 時刻表から発車時刻を取得する。
+ (NSString *)departingHour:(NSTimeInterval)departingAt
{
    // 時刻表から発車時刻を取得する。
    NSDate *departingHour = [NSDate dateWithTimeIntervalSince1970:departingAt];

    // 発車時刻から時のみを取得する。
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // 時刻の表記そのままの時間を取得するために、わざとGMTを指定する。
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"ja_JP"];
    [dateFormatter setLocale:locale];
    [dateFormatter setDateFormat:@"H"];
    NSString *departingHourString = [dateFormatter stringFromDate:departingHour];

    return departingHourString;
}

@end
