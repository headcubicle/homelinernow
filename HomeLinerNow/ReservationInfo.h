//
//  ReservationInfo.h
//  HomeLinerNow
//
//  Created by foobar on 2013/05/16.
//
//

#import <Foundation/Foundation.h>

#import "StationInfo.h"
#import "TrainInfo.h"
#import "UrlEncode.h"
#import "TimeUtil.h"

@interface ReservationInfo : NSObject

@property (nonatomic) TrainInfo *train;
@property (nonatomic) StationInfo *take;
@property (nonatomic) StationInfo *off;
@property NSUInteger person;
@property NSUInteger grade;
@property NSUInteger way;

- (NSString *)httpParameters;
+ (NSUInteger)economy;
+ (NSUInteger)green;
+ (NSUInteger)notSpecified;
+ (NSUInteger)window;
+ (NSUInteger)aisle;

@end
