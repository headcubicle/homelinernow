//
//  ReservationViewController.h
//  HomeLinerNow
//
//  Created by foobar on 2013/02/10.
//
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <libxml/HTMLparser.h>
#import <QuartzCore/QuartzCore.h>
#import "StationList.h"
#import "TimeTable.h"
#import "TrainList.h"
#import "TrainInfo.h"
#import "TimeUtil.h"
#import "HomeLinerNowValues.h"
#import "IndicatorAlertView.h"
#import "DistanceUtil.h"
#import "EkinetAccessor.h"
#import "ReservationInfo.h"
#import "ResultWebView.h"

@interface ReservationViewController : UIViewController <CLLocationManagerDelegate, UITextFieldDelegate, UIActionSheetDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIWebViewDelegate>
{
    CLLocationManager *locationManager;
}

@property (strong, nonatomic) IBOutlet UIView *reservationView;
@property (weak, nonatomic) IBOutlet ResultWebView *resultView;
@property (weak, nonatomic) IBOutlet UITextField *trainNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *fromTextField;
@property (weak, nonatomic) IBOutlet UITextField *forTextField;
@property (weak, nonatomic) IBOutlet UILabel *departingAtLabel;
@property (weak, nonatomic) IBOutlet UILabel *arrivingAtLabel;
@property (copy, nonatomic) StationList *stations;
@property (copy, nonatomic) TrainList *trains;
@property (copy, nonatomic) UIActionSheet *pickerParent;
@property (copy, nonatomic) UIPickerView *trainPicker;
@property (copy, nonatomic) UIPickerView *fromPicker;
@property (copy, nonatomic) UIPickerView *forPicker;
@property (copy, nonatomic) NSArray *trainPickerList;
@property (copy, nonatomic) NSArray *fromPickerList;
@property (copy, nonatomic) NSArray *forPickerList;
@property (copy, nonatomic) CLLocation *userLocation;
@property (copy, nonatomic) ReservationInfo *reservationInfo;
@property (copy, nonatomic) NSOperationQueue *queue;

- (IBAction)reservation:(id)sender;
- (IBAction)trainPickerDoneClick;
- (IBAction)fromPickerDoneClick;
- (IBAction)forPickerDoneClick;
- (IBAction)personChanged:(id)sender;
- (IBAction)gradeChanged:(id)sender;
- (IBAction)wayChanged:(id)sender;
- (void)onResumeLocationManager;
- (void)onPauseLocationManager;
- (void)reservationCancel;
- (void)saveScreenImage;

@end
