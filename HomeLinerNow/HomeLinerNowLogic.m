//
//  HomeLinerNowLogic.m
//  HomeLinerNow
//
//  Created by foobar on 2013/06/07.
//
//

#import "HomeLinerNowLogic.h"

@implementation HomeLinerNowLogic

static HomeLinerNowValues *homeLinerNowValues = nil;

- (id)init
{
    self = [super init];
    
    if(self)
    {
        // 固定値初期化
        homeLinerNowValues = [HomeLinerNowValues sharedManager];
        
        // リスト作成
        NSBundle *bundle = [NSBundle mainBundle];
        // 駅リスト作成
        NSString *stationsPath = [bundle pathForResource:@"Stations" ofType:@"plist"];
        _stations = [[StationList alloc] init:[NSDictionary dictionaryWithContentsOfFile:stationsPath]];
        // 列車リスト作成
        NSString *trainsPath = [bundle pathForResource:@"Trains" ofType:@"plist"];
        _trains = [[TrainList alloc] init:[NSDictionary dictionaryWithContentsOfFile:trainsPath]];
        // ツイート内容初期化
        _aboard = NSLocalizedString(@"orange", @"message for orange");
        // 遅延時分初期化
        _delay = 0.0;
        // 履歴書き込みフラグ初期化
        _isWrite = NO;
        
        return self;
    }
    
    return nil;
}

// 現在の駅を特定
- (NSString *)getCurrentStation:(double)latitude longitude:(double)longitude
{
    NSString *currentStation = @"";
    double currentDistance = homeLinerNowValues.distanceFromStation.doubleValue;
    // 駅リスト取得
    for (NSString *stationName in [_stations allStationNames])
    {
        // 駅の緯度、経度を取得
        StationInfo *stationInfo = [_stations stationInfoFromKey:stationName];
        // 現在位置との誤差を取得
        double distance = [DistanceUtil getDistance:[stationInfo.latitude doubleValue] longitudeFrom:[stationInfo.longitude doubleValue] latitudeTo:latitude longitudeTo:longitude];
        // 誤差が設定値未満であれば採用
        if (distance < [stationInfo.errorRange doubleValue])
        {
            if (currentDistance > distance)
            {
                currentDistance = distance;
                currentStation = stationName;
            }
        }
        NSLog(@"station %@ distance %f\n", stationName, distance);
    }
    
    return currentStation;
}

// 乗車中の列車を特定
- (void)boardingTrainAt:(NSString*)currenntStation
{
    // デフォルトはオレンジの。
    _aboard = NSLocalizedString(@"orange", @"message for orange");
    _history = nil;
    
    // 曜日を取得
    NSDate *date = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:NSWeekdayCalendarUnit fromDate:date];
    
    // 運休日追加したい。
    
    // 平日であれば
    //dateComponents.weekday = 2;
    if (dateComponents.weekday % 7 > 1)
    {
        // 乗車中のライナーを特定
        [self boardingHomeLinerAt:currenntStation];
        // [self boardingHomeLinerAt:@"吉祥寺"];
    }
    
    NSLog(@"abord %@", _aboard);
}

// 乗車中のライナーを特定
- (void)boardingHomeLinerAt:(NSString *)currentStation
{
    int leastTimeDiff = homeLinerNowValues.secondsOfAday.intValue;
    // 現在時刻と発車時刻から列車を特定する。
    for (NSString* trainName in [_trains allTrainNames])
    {
        // 時刻表の発車時刻を取得
        // UNIX時間へ変換
        NSTimeInterval departingTimestamp = [[_trains trainInfoFromKey:trainName] timeTableInfoFromKey:currentStation].departingAt;
        
        // 現在時刻を取得
        NSTimeInterval currentTimestamp = [TimeUtil currentTimestampJst];
        //currentTimestamp = [TimeUtil departingTimestamp:@"22:56"];
        // 時刻以下のみを取得
        double current = fmodl(currentTimestamp, homeLinerNowValues.secondsOfAday.doubleValue);
        NSLog(@"currentTime: %lf", current);
        
        // 現在時刻と発車時刻の差を算出
        double timeDiff = (departingTimestamp +_delay) - current;
        if (timeDiff < homeLinerNowValues.timeRangeOfAboardFrom.doubleValue && timeDiff > homeLinerNowValues.timeRangeOfAboardTo.doubleValue)
        {
            // 差が5分未満の列車に乗車したとみなす。
            _aboard = [trainName stringByAppendingString:NSLocalizedString(@"now", @"message suffix")];
            // 履歴ファイル確認
            _history = [AboardHistory alloc];
            _history.trainName = trainName;
            _history.aboardTime = [[NSNumber alloc] initWithDouble:currentTimestamp];
            // 同一の列車の場合
            [self checkHistoryFile];
            if (!_isWrite) {
                // ぺーぽーぺーぽーぽーぺー
                _aboard = NSLocalizedString(@"horn", @"horn");
            }
            NSLog(@"%@", _aboard);
            break;
        }
        else if (abs(leastTimeDiff) > abs(timeDiff))
        {
            // 乗車駅のみ
            BOOL isAvailable = [[_trains trainInfoFromKey:trainName] timeTableInfoFromKey:currentStation].available;
            NSLog(@"isAvaliable: %d", isAvailable);
            if (isAvailable)
            {
                leastTimeDiff = timeDiff;
                // 逃した
                if (leastTimeDiff <= homeLinerNowValues.timeRangeOfAboardTo.intValue && leastTimeDiff >= homeLinerNowValues.timeRangeOfMissTo.intValue)
                {
                    _aboard = [trainName stringByAppendingString:NSLocalizedString(@"miss", @"suffix for miss")];
                }
                // 待ち
                else if (leastTimeDiff <= homeLinerNowValues.timeRangeOfWaitFrom.intValue && leastTimeDiff >= homeLinerNowValues.timeRangeOfAboardFrom.intValue)
                {
                    _aboard = [trainName stringByAppendingString:NSLocalizedString(@"wait", @"suffix for wait")];
                }
            }
        }
    }
}

// 履歴ファイルチェック
- (void)checkHistoryFile
{
    // 履歴ファイル書き込み要否
    _isWrite = YES;
    
    // cacheディレクトリのパス
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cacheDir = [paths objectAtIndex:0];
    NSString *historyFile = [cacheDir stringByAppendingPathComponent:homeLinerNowValues.historyFileName];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // 履歴ファイルがある場合
    NSLog(@"fileExists: %d", [fileManager fileExistsAtPath:historyFile]);
    if ([fileManager fileExistsAtPath:historyFile])
    {
        AboardHistory *currnetHistory = [NSKeyedUnarchiver unarchiveObjectWithFile:historyFile];
        NSLog(@"currentAbord %@, %lf", currnetHistory.trainName, [currnetHistory.aboardTime doubleValue]);
        // 同一の列車の場合
        if ([currnetHistory.trainName isEqualToString:_history.trainName])
        {
            // 乗車時間の差が1時間以内の場合
            double timeDiff = [_history.aboardTime doubleValue] - [currnetHistory.aboardTime doubleValue];
            NSLog(@"aboardTime: %lf", [_history.aboardTime doubleValue]);
            NSLog(@"currentTime: %lf", [currnetHistory.aboardTime doubleValue]);
            NSLog(@"timeDiff: %lf", timeDiff);
            NSLog(@"boardingTime: %lf", homeLinerNowValues.boardingTime.doubleValue);
            if (timeDiff <= homeLinerNowValues.boardingTime.doubleValue) {
                // 履歴ファイル更新不要
                _isWrite = NO;
                NSLog(@"isWriteChanged %d", _isWrite);
            }
        }
    }
}

// 履歴ファイル書き込み
- (void)writeHistoryFile:(AboardHistory *)aboardHistory
{
    // caeheディレクトリのパス
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cacheDir = [paths objectAtIndex:0];
    NSString *historyFile = [cacheDir stringByAppendingPathComponent:homeLinerNowValues.historyFileName];
    
    // 履歴ファイル更新
    [NSKeyedArchiver archiveRootObject:aboardHistory toFile:historyFile];
    // 履歴ファイル書き込みフラグOFF
    _isWrite = NO;
}

@end
