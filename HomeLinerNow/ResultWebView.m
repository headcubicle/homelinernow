//
//  ResultWebView.m
//  HomeLinerNow
//
//  Created by foobar on 2013/05/26.
//
//

#import "ResultWebView.h"

@implementation ResultWebView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

// 文字列内のHTMLを表示する。
- (void)loadHTMLString:(NSString *)string
{
    [self loadHTMLString:string baseURL:nil];
}

@end
