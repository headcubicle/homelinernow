//
//  IndicatorAlartView.m
//  HomeLinerNow
//
//  Created by foobar on 2013/05/24.
//
//

#import "IndicatorAlertView.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - IndicatorAlertEvent

/**
 * IndicatorAlertView 用のイベント用データを表します。
 */
@interface IndicatorAlertEvent : NSObject

@property (nonatomic, assign) SEL selector; //! コールバック メソッド
@property (nonatomic, assign) id  userInfo; //! コールバックメソッドに指定されるパラメータ

- (id)initWithSelector:(SEL)aSelector userInfo:(id)aUserInfo;

@end

@implementation IndicatorAlertEvent

/**
 * インスタンスを初期化します。
 *
 * @param aSelector セレクター。
 * @param aUserInfo ユーザー データ。
 *
 * @return インスタンス。
 */
- (id)initWithSelector:(SEL)aSelector userInfo:(id)aUserInfo
{
	self = [super init];
	if(self)
	{
		self.selector = aSelector;
		self.userInfo = aUserInfo;
	}
    
	return self;
}

@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - IndicatorAlertView
@interface IndicatorAlertView()

@property (nonatomic, assign) id myDelegate; //! 利用者
@property (nonatomic, retain) NSMutableArray *events; //! イベント情報のコレクション
@property (nonatomic, retain) UIActivityIndicatorView *innerIndicator; //! インジケーター

@end

@implementation IndicatorAlertView

#pragma mark - Lifecycle

/**
 * インスタンスを初期化します。
 *
 * @param title     タイトル。
 * @param message   本文。
 * @param aDelegate デリゲート。
 * @param button    ボタンの文言。
 *
 * @return インスタンス。
 */
- (id)initWithButton:(NSString *)title message:(NSString *)message delegate:(id)aDelegate button:(NSString *)button
{
	self = [super initWithTitle:title message:message delegate:nil cancelButtonTitle:button otherButtonTitles:nil];
	if(self)
	{
		self.delegate   = self;
		self.myDelegate = aDelegate;
		self.events     = [[NSMutableArray alloc] init];
        
		if([self hasCancelButton])
		{
			[self setFirstButtonDelegate:nil userInfo:nil];
		}
	}
    
	return self;
}

/**
 * インジケーター付きアラートのインスタンスを初期化します。
 *
 * @param title     タイトル。
 * @param message   本文。
 * @param aDelegate デリゲート。
 * @param button    ボタンの文言。
 *
 * @return インスタンス。
 */
- (id)initWithIndicator:(NSString *)title message:(NSString *)message delegate:(id)aDelegate button:(NSString *)button
{
	self = [self initWithButton:title message:message delegate:aDelegate button:button];
	if(self)
	{
		self.innerIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(125, 80, 30, 30)];
		self.innerIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
		[self addSubview:self.innerIndicator];
		[self.innerIndicator startAnimating];
	}
    
	return self;
}

/**
 * ボタン付きアラートのインスタンスを生成します。
 *
 * @param title    タイトル。
 * @param message  本文。
 * @param delegate デリゲート。
 * @param button   ボタンの文言。
 *
 * @return インスタンス。
 */
+ (IndicatorAlertView *)alertWithButton:(NSString *)title message:(NSString *)message delegate:(id)delegate button:(NSString *)button
{
	return [[IndicatorAlertView alloc] initWithButton:title message:message delegate:delegate button:button];
}

/**
 * インジケーター付きアラートのインスタンスを生成します。
 *
 * @param title    タイトル。
 * @param message  本文。
 * @param delegate デリゲート。
 * @param button   ボタンの文言。
 *
 * @return インスタンス。
 */
+ (IndicatorAlertView *)alertWithIndicator:(NSString *)title message:(NSString *)message delegate:(id)delegate button:(NSString *)button
{
	return [[IndicatorAlertView alloc] initWithIndicator:title message:message delegate:delegate button:button];
}

#pragma mark - Override

/**
 * アラートを表示します。
 */
- (void)show
{
    [super show];
    if(self.innerIndicator)
    {
        [self.innerIndicator startAnimating];
    }
}

#pragma mark - Public

/**
 * ボタンを追加します。
 *
 * @param title    ボタンのタイトル。
 * @param selector コールバック メソッド
 * @param userInfo コールバック メソッドに指定されるパラメータ。
 */
- (void)addButtonWithTitle:(NSString *)title selector:(SEL)selector userInfo:(id)userInfo
{
	[self addButtonWithTitle:title];
    
	IndicatorAlertEvent* event = [[IndicatorAlertEvent alloc] initWithSelector:selector userInfo:userInfo];
	[self.events addObject:event];
}

/**
 * アラートを閉じます。
 *
 * @param buttonIndex 閉じる時に押されたボタンのインデックス。ボタンの無いアラートの場合は 0 を指定します。
 */
- (void)close:(NSInteger)buttonIndex
{
	[self dismissWithClickedButtonIndex:buttonIndex animated:YES];
}

/**
 * はじめのボタンが押された時のハンドラを設定します。
 *
 * @param selector コールバック メソッド
 * @param userInfo コールバック メソッドに指定されるパラメータ。
 */
- (void)setFirstButtonDelegate:(SEL)selector userInfo:(id)userInfo
{
	if([self hasCancelButton])
	{
		if(self.events.count > 0)
		{
			[self.events removeObjectAtIndex:0];
		}
        
		IndicatorAlertEvent *event = [[IndicatorAlertEvent alloc] initWithSelector:selector userInfo:userInfo];
		[self.events insertObject:event atIndex:0];
	}
}

#pragma mark - UIAlertViewDelegate

/**
 * UIAlertView 上のボタンがクリックされた時に発生します。
 *
 * @param alertView   操作の行われた UIAlertView。
 * @param buttonIndex ボタンのインデックス。
 */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	IndicatorAlertEvent *event = [self.events objectAtIndex:buttonIndex];
	if(event.selector && [self.myDelegate respondsToSelector:event.selector])
	{
		[self.myDelegate performSelector:event.selector withObject:event.userInfo afterDelay:0.0f];
	}
}

/**
 * UIAlertView が表示される時に発生します。
 *
 * @param alertView 表示対象となる UIAlertView。
 */
- (void)willPresentAlertView:(UIAlertView *)alertView
{
	// ボタンが皆無なら、表示関連の調整は不要
	if(self.numberOfButtons < 1)
    {
        return;
    }
    
	NSInteger offsetY = 0;
	NSInteger offsetH = 0;
    
	if(self.innerIndicator)
	{
		offsetY = self.innerIndicator.frame.origin.y;
		offsetH = self.innerIndicator.frame.size.height;
	}
	else
	{
		return;
	}
    
	const NSInteger padding = 8;
    
	// アラート本体の位置とサイズを調整。
	// 垂直位置を追加したコントロールの半分、ずらすことで中央としている。
	// ただし、小数点以下の値が混じると描画がぼやけるため、offsetH を意図的に NSInteger で宣言している。
	//
	CGRect frame = self.frame;
	frame.origin.y    -= offsetH / 2;
	frame.size.height += offsetH + padding;
	self.frame = frame;
    
	// 追加コントロール以下のものを位置調整
	for(UIView * view in self.subviews)
	{
		frame = view.frame;
		if(offsetY < frame.origin.y)
		{
			frame.origin.y += offsetH + padding;
			view.frame = frame;
		}
	}
}

#pragma mark - Private

/**
 * キャンセル ボタンが設定されていることを調べます。
 *
 * @return 設定されている場合は YES。それ以外は NO。
 */
- (BOOL)hasCancelButton
{
	return (self.cancelButtonIndex == 0);
}

@end