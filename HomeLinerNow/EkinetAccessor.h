//
//  EkinetAccessor.h
//  HomeLinerNow
//
//  Created by foobar on 2013/03/30.
//
//

#import <Foundation/Foundation.h>
#import <libxml/HTMLparser.h>
#import "UrlEncode.h"

@interface EkinetAccessor : NSObject

@property NSString *ekinetUrl;
@property NSString *userAgent;

- (id)init;
- (NSString *)openEkinet;
- (NSString *)openLogin:(NSString *)loginUrlString;
- (NSString *)login:(NSString *)loginFormString;
- (NSString *)openReservationTop:(NSString *)reservationTopString;
- (NSString *)openTrainSearch:(NSString *)reservationString;
- (NSString *)openTrainSelect:(NSString *)trainSelectUrlString requsetBody:(NSString *)requestBody trainNumberString:(NSString *)trainNumberString;
- (NSString *)openConfirm:(NSString *)confirmUrlString;
- (NSString *)reservation:(NSString *)reservationUrlString;

@end
