//
//  TrainList.h
//  HomeLinerNow
//
//  Created by foobar on 2012/12/31.
//
//

#import <Foundation/Foundation.h>
#import "TrainInfo.h"

@interface TrainList : NSObject

@property (copy, nonatomic) NSMutableDictionary *trains;

- (id)init:(NSDictionary *)trains;
- (NSArray *)allTrainNames;
- (TrainInfo *)trainInfoFromKey:(NSString *)key;

@end
