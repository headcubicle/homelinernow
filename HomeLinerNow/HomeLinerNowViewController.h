//
//  HomeLinerNowViewController.h
//  HomeLinerNow
//
//  Created by foobar on 2012/12/02.
//
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import <CoreLocation/CoreLocation.h>
#import <math.h>
#import "AboardHistory.h"
#import "TimeUtil.h"
#import "HomeLinerNowValues.h"
#import "HomeLinerNowLogic.h"

@interface HomeLinerNowViewController : UIViewController <CLLocationManagerDelegate, UITextFieldDelegate, UIActionSheetDelegate>
{
    CLLocationManager *locationManager;
}

@property (weak, nonatomic) IBOutlet UITextField *delayTextField;
@property (weak, nonatomic) IBOutlet UILabel *delayLabel;
@property (copy, nonatomic) CLLocation *userLocation;
@property (copy, nonatomic) UIActionSheet *delayPickerParent;
@property (copy, nonatomic) UIDatePicker *delayPicker;
@property (copy, nonatomic) HomeLinerNowLogic* logic;

- (IBAction)tweetLinerNow:(id)sender;
- (IBAction)resetDelay:(id)sender;
- (IBAction)delayPickerDoneClick;
- (void)onResumeLocationManager;
- (void)onPauseLocationManager;

@end
