//
//  ReservationViewController.m
//  HomeLinerNow
//
//  Created by foobar on 2013/02/10.
//
//

#import "ReservationViewController.h"

@interface ReservationViewController()

@end

@implementation ReservationViewController

static HomeLinerNowValues *homeLinerNowValues = nil;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if(self)
    {
        // 固定値初期化
        homeLinerNowValues = [HomeLinerNowValues sharedManager];
        // リスト作成
        NSBundle *bundle = [NSBundle mainBundle];
        // 駅リスト作成
        NSString *stationsPath = [bundle pathForResource:@"Stations" ofType:@"plist"];
        _stations = [[StationList alloc] init:[NSDictionary dictionaryWithContentsOfFile:stationsPath]];
        // 列車リスト作成
        NSString *trainsPath = [bundle pathForResource:@"Trains" ofType:@"plist"];
        _trains = [[TrainList alloc] init:[NSDictionary dictionaryWithContentsOfFile:trainsPath]];
        
        return self;
    }
    
    return nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 予約結果画面を非表示とする。
    _resultView.hidden = YES;
    
    // ResultWebViewのdelegateを設定する。
    [_resultView setDelegate:self];
    
    // 位置情報サービス利用可否チェック
    if (locationManager == nil)
    {
        locationManager = [[CLLocationManager alloc] init];
        NSLog(@"CLLocationManager is allocated");
    }
    
    locationManager.delegate = self;
    
    // 測位開始
    [self onResumeLocationManager];

    // 列車リストを作成する。
    [self createTrainList];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 列車リストを作成する
- (void)createTrainList
{
    NSBundle *bundle = [NSBundle mainBundle];
    // 列車リストから予約可能列車を抽出
    NSString *trainsPath = [bundle pathForResource:@"Trains" ofType:@"plist"];
    _trains = [[TrainList alloc] init:[NSDictionary dictionaryWithContentsOfFile:trainsPath]];
    
    // 現在時刻を取得
    //NSTimeInterval currentTimestamp = [TimeUtil departingTimestamp:@"19:10"];
    NSTimeInterval currentTimestamp = [TimeUtil currentTimestampJst];
    // 時刻以下のみを取得
    double current = fmodl(currentTimestamp, homeLinerNowValues.secondsOfAday.doubleValue);
    NSLog(@"current: %lf", current);
    
    NSMutableArray *tempTrainList = [NSMutableArray array];
    for (NSString *trainName in [_trains allTrainNames])
    {
        TrainInfo *trainInfo = [_trains trainInfoFromKey:trainName];
        // えきねっと予約可能
        if (trainInfo.ekinetAvailable)
        {
            // 直近の乗車駅を確認する。
            NSString *boardingAt = [self currentStartSation:trainName currentTime:current];
            // 乗車可能な場合はリストに追加する。
            if (boardingAt != nil)
            {
                [tempTrainList addObject:trainInfo];
            }
        }
    }
    
    // 列車を時刻順にソート
    _trainPickerList = [tempTrainList sortedArrayUsingFunction:compareTrain context:nil];
    // 列車リストが空でない場合
    if (_trainPickerList.count > 0)
    {
        TrainInfo *trainInfo = (TrainInfo *)[_trainPickerList objectAtIndex:0];
        // リストの先頭の列車をtextfieldに表示する。
        NSString *trainName = [trainInfo trainName];
        _trainNameTextField.text = trainName;
        // 最も近い乗車駅をtextfeildに表示する。
        NSString *boardingAt = [self currentStartSation:trainName currentTime:current];
        _fromTextField.text = boardingAt;
        // 乗車駅をpicker用リストに追加する。
        _fromPickerList = [self availableStation:trainName currentTime:current];
        // 発車時刻をlabelに表示する。
        _departingAtLabel.text = [NSString stringWithFormat:NSLocalizedString(@"departingAt", @"departingAt"), [(TimeTable *)[[trainInfo timeTable] valueForKey:boardingAt] departure]];
        
        // 終着駅をtextfieldに表示する。
        NSArray *timeTableFor = [[trainInfo timeTable] allValues];
        timeTableFor = [timeTableFor sortedArrayUsingFunction:compareTime context:nil];
        TimeTable *arrivingAt = (TimeTable *)[timeTableFor objectAtIndex:(timeTableFor.count - 1)];
        _forTextField.text = [arrivingAt stationName];
        // 停車駅をpicker用リストに追加する。
        _forPickerList = [self stopStation:trainName];
        // 到着時刻をlabelに表示する。
        _arrivingAtLabel.text = [NSString stringWithFormat:NSLocalizedString(@"arrivingAt", @"arrivingAt"), [arrivingAt departure]];
        
        // 予約情報を設定する。
        _reservationInfo = [ReservationInfo alloc];
        _reservationInfo.train = [_trains trainInfoFromKey:trainName];
        _reservationInfo.take = [_stations stationInfoFromKey:boardingAt];
        _reservationInfo.off = [_stations stationInfoFromKey:arrivingAt.stationName];
        _reservationInfo.person = 1;
        _reservationInfo.grade = [ReservationInfo economy];
        _reservationInfo.way = [ReservationInfo notSpecified];
        NSLog(@"ReservationInfo=%@", [_reservationInfo httpParameters]);
    }
}

// 直近の乗車駅を返す。
- (NSString *)currentStartSation:(NSString *)trainName currentTime:(double)currentTime
{
    TrainInfo *trainInfo = [_trains trainInfoFromKey:trainName];
    NSArray *timeTable = [[trainInfo timeTable] allValues];
    timeTable = [timeTable sortedArrayUsingFunction:compareTime context:nil];
    NSString *boardingAt = nil;
    double currentDistance = -1.0;
    
    // 時刻表を確認する。
    for (TimeTable *stationTimeTable in timeTable)
    {
        NSLog(@"timeTable: %@", stationTimeTable.stationName);
        // 乗車可能かつ発車時刻を過ぎていない場合は、直近の乗車駅とする。
        if (stationTimeTable.available && stationTimeTable.departingAt > currentTime)
        {
            // 乗車駅までの距離を計算
            double distance = [DistanceUtil getDistance:[_stations stationInfoFromKey:stationTimeTable.stationName].latitude.doubleValue longitudeFrom:[_stations stationInfoFromKey:stationTimeTable.stationName].longitude.doubleValue latitudeTo:_userLocation.coordinate.latitude longitudeTo:_userLocation.coordinate.longitude];
            
            // 距離の近い駅を乗車駅とする。
            if (currentDistance < 0.0 || currentDistance > distance)
            {
                currentDistance = distance;
                boardingAt = stationTimeTable.stationName;
                NSLog(@"boardingAt: %@", boardingAt);
            }
        }
    }
    
    return boardingAt;
}

// 乗車可能駅を返す。
- (NSArray *)availableStation:(NSString *)trainName currentTime:(double)currentTime
{
    TrainInfo *trainInfo = [_trains trainInfoFromKey:trainName];
    NSArray *timeTable = [[trainInfo timeTable] allValues];
    timeTable = [timeTable sortedArrayUsingFunction:compareTime context:nil];
    NSMutableArray *availableStationArray = [NSMutableArray array];

    // 時刻表を確認する。
    for (TimeTable *stationTimeTable in timeTable)
    {
        NSLog(@"timeTable: %@", stationTimeTable.stationName);
        // 乗車可能かつ発車時刻を過ぎていない場合は、直近の乗車駅とする。
        if (stationTimeTable.available && stationTimeTable.departingAt > currentTime)
        {
            [availableStationArray addObject:stationTimeTable];
        }
    }
    
    return availableStationArray;
}

// 停車駅を返す。
- (NSArray *)stopStation:(NSString *)trainName
{
    TrainInfo *trainInfo = [_trains trainInfoFromKey:trainName];
    NSArray *timeTable = [[trainInfo timeTable] allValues];
    timeTable = [timeTable sortedArrayUsingFunction:compareTime context:nil];
    NSMutableArray *stopStationArray = [NSMutableArray array];
    
    // 時刻表を確認する。
    for (TimeTable *stationTimeTable in timeTable)
    {
        // 停車駅の場合、追加する。
        if (stationTimeTable.willStop)
        {
            NSLog(@"willStop: %@", stationTimeTable.stationName);
            [stopStationArray addObject:stationTimeTable];
        }
    }
    
    return stopStationArray;
}

// 列車ソート用コンパレータ
NSInteger compareTrain(id info1, id info2, void *context)
{
    NSArray *train1 = [[(TrainInfo *)info1 timeTable] allValues];
    NSArray *train2 = [[(TrainInfo *)info2 timeTable] allValues];
    NSInteger status = NSOrderedSame;

    // 時刻表をソート
    train1 = [train1 sortedArrayUsingFunction:compareTime context:nil];
    train2 = [train2 sortedArrayUsingFunction:compareTime context:nil];
    
    if ([[train1 objectAtIndex:0] departingAt] < [[train2 objectAtIndex:0] departingAt])
    {
        status = NSOrderedAscending;
    }
    else if ([[train1 objectAtIndex:0] departingAt] > [[train2 objectAtIndex:0] departingAt])
    {
        status = NSOrderedDescending;
    }
    
    return status;
}

// 時刻表ソート用コンパレータ
NSInteger compareTime(id info1, id info2, void *context)
{
    NSTimeInterval time1 = [(TimeTable *)info1 departingAt];
    NSTimeInterval time2 = [(TimeTable *)info2 departingAt];
    NSInteger status = NSOrderedSame;
    
    if (time1 < time2)
    {
        status = NSOrderedAscending;
    }
    else if (time1 > time2)
    {
        status = NSOrderedDescending;
    }
    
    return status;
}

// pickerのコンポーネント数
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// pickerの行数を設定する。
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger rows = 0;

    // 列車pickerの場合
    if (pickerView == _trainPicker)
    {
        rows = _trainPickerList.count;
    }
    else if (pickerView == _fromPicker)
    {
        rows = _fromPickerList.count;
    }
    else if (pickerView == _forPicker)
    {
        rows = _forPickerList.count;
    }
    
    return rows;
}

// pickerに列車名、乗車駅、降車駅を設定する。
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView == _trainPicker)
    {
        return [[_trainPickerList objectAtIndex:row] trainName];
    }
    else if (pickerView == _fromPicker)
    {
        return [[_fromPickerList objectAtIndex:row] stationName];
    }
    else
    {
        return [[_forPickerList objectAtIndex:row] stationName];
    }
}

// TextField編集開始
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    // ActionSheetを準備
    _pickerParent = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil, nil];

    // Toolbarを作成
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 44.0)];
    toolbar.barStyle = UIBarStyleBlackOpaque;
    [toolbar sizeToFit];
    NSMutableArray *toolbarItems = [[NSMutableArray alloc] init];

    // 列車選択
    if (textField == _trainNameTextField)
    {
        // UIPickerViewを作成
        _trainPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, 44.0, 0.0, 0.0)];
        _trainPicker.showsSelectionIndicator = YES;
        _trainPicker.delegate = self;
        _trainPicker.dataSource = self;
        
        UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(trainPickerDoneClick)];
        [toolbarItems addObject:flexSpace];
        [toolbar setItems:toolbarItems animated:YES];
        // toolbarとpickerを追加
        [_pickerParent addSubview:toolbar];
        [_pickerParent addSubview:_trainPicker];
    }
    // 乗車駅選択
    else if (textField == _fromTextField)
    {
        // UIPickerViewを作成
        _fromPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, 44.0, 0.0, 0.0)];
        _fromPicker.showsSelectionIndicator = YES;
        _fromPicker.delegate = self;
        _fromPicker.dataSource = self;
        
        UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(fromPickerDoneClick)];
        [toolbarItems addObject:flexSpace];
        [toolbar setItems:toolbarItems animated:YES];
        // toolbarとpickerを追加
        [_pickerParent addSubview:toolbar];
        [_pickerParent addSubview:_fromPicker];
    }
    // 降車駅選択
    else if (textField == _forTextField)
    {
        // UIPickerViewを作成
        _forPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, 44.0, 0.0, 0.0)];
        _forPicker.showsSelectionIndicator = YES;
        _forPicker.delegate = self;
        _forPicker.dataSource = self;
        
        UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(forPickerDoneClick)];
        [toolbarItems addObject:flexSpace];
        [toolbar setItems:toolbarItems animated:YES];
        // toolbarとpickerを追加
        [_pickerParent addSubview:toolbar];
        [_pickerParent addSubview:_forPicker];
    }
    
    [_pickerParent showInView:self.view];
    [_pickerParent setBounds:CGRectMake(0.0, 0.0, 320.0, 464.0)];

    // キーボードの表示を停止する。
    return NO;
}

// 予約
- (IBAction)reservation:(id)sender
{
    IndicatorAlertView *alert = [IndicatorAlertView alertWithIndicator:@"予約処理中" message:@"しばらくお待ちください。" delegate:self button:@"中止"];
    [alert setFirstButtonDelegate:@selector(reservationCancel) userInfo:@"reservationCancel"];

    if (!_queue) {
        _queue = [[NSOperationQueue alloc] init];
    }
    
    NSBlockOperation *operation = [[NSBlockOperation alloc] init];
    __weak NSBlockOperation *weakOperation = operation;

    [operation addExecutionBlock:^{
        EkinetAccessor *ekinetAccessor = [[EkinetAccessor alloc] init];

        // えきねっとTOPを表示する。
        if(weakOperation.isCancelled) {
            return;
        }
        NSString *loginUrl = [ekinetAccessor openEkinet];
        // ログイン画面を表示する。
        if(weakOperation.isCancelled) {
            return;
        }
        NSString *loginForm = [ekinetAccessor openLogin:loginUrl];
        // ログインする
        if(weakOperation.isCancelled) {
            return;
        }
        NSString *reservationTop = [ekinetAccessor login:loginForm];
        // ライナー予約画面TOPを表示する。
        if(weakOperation.isCancelled) {
            return;
        }
        NSString *trainSearch = [ekinetAccessor openReservationTop:reservationTop];
        // 列車検索画面を表示する。
        if(weakOperation.isCancelled) {
            return;
        }
        NSString *trainSelect = [ekinetAccessor openTrainSearch:trainSearch];
        // 列車選択画面を表示する。
        if(weakOperation.isCancelled) {
            return;
        }
        NSString *confirmUrl = [ekinetAccessor openTrainSelect:trainSelect requsetBody:[_reservationInfo httpParameters] trainNumberString:_reservationInfo.train.trainNumber];
        // 確認画面を表示する。
        if(weakOperation.isCancelled) {
            return;
        }
        NSString *reservation = [ekinetAccessor openConfirm:confirmUrl];
        // 予約を確定する。
        if(weakOperation.isCancelled) {
            return;
        }
        NSString *result = [ekinetAccessor reservation:reservation];
        
        // 予約結果を開く。
        [_resultView performSelectorInBackground:@selector(loadHTMLString:) withObject:result];

        // ダイアログを消去する。
        [alert performSelectorInBackground:@selector(close:) withObject:0];
    }];
    
    // 予約処理を実行する。
    [_queue addOperation:operation];
    // 予約処理中ダイアログを表示する。
    [alert show];

    // 予約結果画面を表示する。
    _resultView.hidden = NO;
}

// 予約処理中断
- (void)reservationCancel
{
    [_queue cancelAllOperations];
    NSLog(@"reservation canceled.");
}

// WebViewロード完了後処理
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    // スクリーンショットを撮る。
    [self performSelector:@selector(saveScreenImage) withObject:nil afterDelay:1.0];
}

// 予約結果画面のスクリーンショットを撮る。
- (void)saveScreenImage
{
    UIGraphicsBeginImageContextWithOptions(_resultView.scrollView.contentSize, _resultView.scrollView.opaque, 0);
    
    CGPoint savedContentOffset = _resultView.scrollView.contentOffset;
    CGRect savedFrame = _resultView.scrollView.frame;
    
    _resultView.scrollView.contentOffset = CGPointZero;
    _resultView.scrollView.frame = CGRectMake(0, 0, _resultView.scrollView.contentSize.width, _resultView.scrollView.contentSize.height);
    
    [_resultView.scrollView.layer renderInContext:UIGraphicsGetCurrentContext()];
    NSData *data = UIImagePNGRepresentation(UIGraphicsGetImageFromCurrentImageContext());
    UIImage *capture = [UIImage imageWithData:data];
    UIImageWriteToSavedPhotosAlbum(capture, nil, nil, nil);
    
    _resultView.scrollView.contentOffset = savedContentOffset;
    _resultView.scrollView.frame = savedFrame;
    
    UIGraphicsEndImageContext();
}

// 列車選択完了
- (IBAction)trainPickerDoneClick
{
    // pickerから列車名を取得してtextfieldに表示する。
    NSInteger selected = [_trainPicker selectedRowInComponent:0];
    _trainNameTextField.text = [(TrainInfo *)[_trainPickerList objectAtIndex:selected] trainName];
    
    // 現在時刻を取得
    //NSTimeInterval currentTimestamp = [TimeUtil departingTimestamp:@"19:10"];
    NSTimeInterval currentTimestamp = [TimeUtil currentTimestampJst];
    // 時刻以下のみを取得
    double current = fmodl(currentTimestamp, homeLinerNowValues.secondsOfAday.doubleValue);
    
    // 最も近い乗車駅をtextfeildに表示する。
    NSString *boardingAt = [self currentStartSation:[(TrainInfo *)[_trainPickerList objectAtIndex:selected] trainName] currentTime:current];
    _fromTextField.text = boardingAt;
    // 乗車駅をpicker用リストに追加する。
    _fromPickerList = [self availableStation:[(TrainInfo *)[_trainPickerList objectAtIndex:selected] trainName] currentTime:current];
    // 発車時刻をlabelに表示する。
    _departingAtLabel.text = [NSString stringWithFormat:NSLocalizedString(@"departingAt", @"departingAt"), [(TimeTable *)[[(TrainInfo *)[_trainPickerList objectAtIndex:selected] timeTable] valueForKey:boardingAt] departure]];
    
    // 終着駅をtextfieldに表示する。
    NSArray *timeTableFor = [[(TrainInfo *)[_trainPickerList objectAtIndex:selected] timeTable] allValues];
    timeTableFor = [timeTableFor sortedArrayUsingFunction:compareTime context:nil];
    TimeTable *arrivingAt = (TimeTable *)[timeTableFor objectAtIndex:(timeTableFor.count - 1)];
    _forTextField.text = [arrivingAt stationName];
    // 停車駅をpicker用リストに追加する。
    _forPickerList = [self stopStation:[(TrainInfo *)[_trainPickerList objectAtIndex:selected] trainName]];
    // 到着時刻をlabelに表示する。
    _arrivingAtLabel.text = [NSString stringWithFormat:NSLocalizedString(@"arrivingAt", @"arrivingAt"), [arrivingAt departure]];
    
    // 予約情報を変更する。
    _reservationInfo.train = (TrainInfo *)[_trainPickerList objectAtIndex:selected];
    _reservationInfo.take = [_stations stationInfoFromKey:boardingAt];
    _reservationInfo.off = [_stations stationInfoFromKey:arrivingAt.stationName];
    NSLog(@"ReservationInfo=%@", [_reservationInfo httpParameters]);
    
    [_pickerParent dismissWithClickedButtonIndex:0 animated:YES];
    [_trainNameTextField resignFirstResponder];
}

// 乗車駅選択完了
- (IBAction)fromPickerDoneClick
{
    // pickerから乗車駅を取得してtextfieldに表示する。
    NSInteger selected = [_fromPicker selectedRowInComponent:0];
    _fromTextField.text = [(TimeTable *)[_fromPickerList objectAtIndex:selected] stationName];
    // 発車時刻をlabelに設定する。
    _departingAtLabel.text = [NSString stringWithFormat:NSLocalizedString(@"departingAt", @"departingAt"), [(TimeTable *)[_fromPickerList objectAtIndex:selected] departure]];

    // 予約情報を変更する。
    _reservationInfo.take = [_stations stationInfoFromKey:[(TimeTable *)[_fromPickerList objectAtIndex:selected] stationName]];
    NSLog(@"ReservationInfo=%@", [_reservationInfo httpParameters]);
    
    [_pickerParent dismissWithClickedButtonIndex:0 animated:YES];
    [_fromTextField resignFirstResponder];
}

// 降車駅選択完了
- (IBAction)forPickerDoneClick
{
    // pickerから降車駅を取得してtextfieldに表示する。
    NSInteger selected = [_forPicker selectedRowInComponent:0];
    _forTextField.text = [(TimeTable *)[_forPickerList objectAtIndex:selected] stationName];
    // 発車時刻をlabelに設定する。
    _arrivingAtLabel.text = [NSString stringWithFormat:NSLocalizedString(@"arrivingAt", @"arrivingAt"), [(TimeTable *)[_forPickerList objectAtIndex:selected] departure]];
    
    // 予約情報を変更する。
    _reservationInfo.off = [_stations stationInfoFromKey:[(TimeTable *)[_forPickerList objectAtIndex:selected] stationName]];
    NSLog(@"ReservationInfo=%@", [_reservationInfo httpParameters]);

    [_pickerParent dismissWithClickedButtonIndex:0 animated:YES];
    [_forTextField resignFirstResponder];
}

// 人数変更
- (IBAction)personChanged:(id)sender
{
    UISegmentedControl *personSegment = sender;
    
    // 予約情報に人数を設定する。
    switch (personSegment.selectedSegmentIndex)
    {
        case 0:
            _reservationInfo.person = 1;
            break;
        case 1:
            _reservationInfo.person = 2;
            break;
        case 2:
            _reservationInfo.person = 3;
            break;
        case 3:
            _reservationInfo.person = 4;
            break;
        // デフォルトは1名とする。
        default:
            _reservationInfo.person = 1;
            break;
    }
    
    NSLog(@"ReservationInfo=%@", [_reservationInfo httpParameters]);
}

// 座席種別変更
- (IBAction)gradeChanged:(id)sender
{
    UISegmentedControl *gradeSegment = sender;
    
    // 予約情報に座席種別を設定する。
    switch (gradeSegment.selectedSegmentIndex)
    {
        // 普通車
        case 0:
            _reservationInfo.grade = [ReservationInfo economy];
            break;
        // グリーン車
        case 1:
            _reservationInfo.grade = [ReservationInfo green];
            break;
        // デフォルトは普通車とする。
        default:
            _reservationInfo.grade = [ReservationInfo economy];
            break;
    }
    
    NSLog(@"ReservationInfo=%@", [_reservationInfo httpParameters]);
}

// 座席位置変更
- (IBAction)wayChanged:(id)sender
{
    UISegmentedControl *waySegment = sender;
    
    // 予約情報に座席位置を設定する。
    switch (waySegment.selectedSegmentIndex)
    {
        // 指定無し
        case 0:
            _reservationInfo.way = [ReservationInfo notSpecified];
            break;
        // 窓側
        case 1:
            _reservationInfo.way = [ReservationInfo window];
            break;
        // 通路側
        case 2:
            _reservationInfo.way = [ReservationInfo aisle];
            break;
        // デフォルトは指定無しとする。
        default:
            _reservationInfo.way = [ReservationInfo notSpecified];
            break;
    }
    
    NSLog(@"ReservationInfo=%@", [_reservationInfo httpParameters]);
}

// 測位失敗
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError");
}

// 測位開始
- (void)onResumeLocationManager
{
    // 出来れば大幅変更位置情報サービスを利用したい。
    if (locationManager != nil)
    {
        if ([CLLocationManager significantLocationChangeMonitoringAvailable])
        {
            [locationManager startMonitoringSignificantLocationChanges];
            NSLog(@"Significant location change monitoring is available");
        }
        else if ([CLLocationManager locationServicesEnabled])
        {
            [locationManager startUpdatingLocation];
            NSLog(@"Location services is available");
        }
        _userLocation = locationManager.location;
        NSLog(@"latitude %+.6f, longitude %+.6f\n", _userLocation.coordinate.latitude, _userLocation.coordinate.longitude);
        /*
         if ([CLLocationManager locationServicesEnabled])
         {
         [locationManager startUpdatingLocation];
         NSLog(@"Location services is available");
         }
         */
    }
}

// 測位停止
- (void)onPauseLocationManager
{
    if (locationManager != nil)
    {
        // 出来れば大幅変更位置情報サービスを利用したい。
        if ([CLLocationManager significantLocationChangeMonitoringAvailable])
        {
            [locationManager stopMonitoringSignificantLocationChanges];
            NSLog(@"Significant location change monitoring stoped");
        }
        else if ([CLLocationManager locationServicesEnabled])
        {
            [locationManager stopUpdatingLocation];
            NSLog(@"Location services stoped");
        }
        /*
         if ([CLLocationManager locationServicesEnabled])
         {
         [locationManager stopUpdatingLocation];
         NSLog(@"Location services stoped");
         }
         */
    }
}

@end
