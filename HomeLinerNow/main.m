//
//  main.m
//  HomeLinerNow
//
//  Created by foobar on 2012/12/02.
//
//

#import <UIKit/UIKit.h>

#import "HomeLinerNowAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HomeLinerNowAppDelegate class]));
    }
}
