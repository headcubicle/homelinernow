//
//  StationInfo.m
//  HomeLinerNow
//
//  Created by foobar on 2012/12/02.
//
//

#import "StationInfo.h"

@implementation StationInfo

- (id)init:(NSString *)name latitude:(NSNumber *)latitude longitude:(NSNumber *)longitude errorRange:(NSNumber *)errorRange stationId:(NSString *)stationId
{
    self = [super init];
    
    if (self)
    {
        _name = name;
        _latitude = latitude;
        _longitude = longitude;
        _errorRange = errorRange;
        _stationId = stationId;
        
        return self;
    }
    
    return nil;
}

@end
