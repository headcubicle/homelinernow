//
//  HomeLinerNowViewController.m
//  HomeLinerNow
//
//  Created by foobar on 2012/12/02.
//
//

#import "HomeLinerNowViewController.h"

@interface HomeLinerNowViewController ()

@end

@implementation HomeLinerNowViewController

static HomeLinerNowValues *homeLinerNowValues = nil;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if(self)
    {
        // 固定値初期化
        homeLinerNowValues = [HomeLinerNowValues sharedManager];
        
        // ロジック初期化
        _logic = [[HomeLinerNowLogic alloc] init];
        
        return self;
    }
    
    return nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 位置情報サービス利用可否チェック
    if (locationManager == nil)
    {
        locationManager = [[CLLocationManager alloc] init];
        NSLog(@"CLLocationManager is allocated");
    }

    locationManager.delegate = self;

    // 測位開始
    [self onResumeLocationManager];
    
    // 「戻る」ボタンのタイトルを設定する。
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] init];
    backBarButtonItem.title = @"戻る";
    [self.navigationItem setBackBarButtonItem:backBarButtonItem];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tweetLinerNow:(id)sender
{
    // 駅取得
    NSString *currentStation = [_logic getCurrentStation:_userLocation.coordinate.latitude longitude:_userLocation.coordinate.longitude];
    NSLog(@"currnetStation: %@", currentStation);
    
    // 駅を特定できなかったら
    if ([currentStation isEqualToString:@""])
    {
        // ダイアログ表示
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"locationWarningTitle", @"dialog title")
                              message:NSLocalizedString(@"locationWarningMessage", @"message")
                              delegate:nil
                              cancelButtonTitle:nil
                              otherButtonTitles:NSLocalizedString(@"locationWarningButton", @"button"),
                              nil];
        [alert show];
    }
    else
    {
        // Tweet
        [_logic boardingTrainAt:currentStation];
        [self Tweet:_logic.aboard];
    }
}

// 遅延時分を初期化する。
- (IBAction)resetDelay:(id)sender
{
    // 遅延時分textfieldを初期化する。
    _delayTextField.text = @"";
    // 遅延時分を初期化する。
    _logic.delay = 0.0;
}

// Tweet
- (void)Tweet:(NSString *)contents
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        ACAccountStore *accountStore = [[ACAccountStore alloc] init];
        ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
        [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error)
        {
            if (granted)
            {
                NSArray *accountArray = [accountStore accountsWithAccountType:accountType];
                if (accountArray.count > 0)
                {
                    NSURL *url = [NSURL URLWithString:homeLinerNowValues.twitterApiUrl];
                    NSDictionary *params = [NSDictionary dictionaryWithObject:contents forKey:homeLinerNowValues.twitterApiParameterKey];
                    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:url parameters:params];
                    [request setAccount:[accountArray objectAtIndex:0]];
                    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
                    {
                        NSLog(@"responseData=%@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
                        NSLog(@"urlResponse.statusCode=%d", urlResponse.statusCode);
                        // 正常にTweetできた場合、履歴ファイルを書き込む。
                        if (responseData != nil) {
                            if (urlResponse.statusCode == 200 && _logic.isWrite)
                            {
                                [_logic writeHistoryFile:_logic.history];
                            }
                        }
                    }];
                }
            }
        }];
    }
    else
    {
        NSLog(@"Twitter account is not available");
    }
}

// ユーザ位置情報更新
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
    NSDate *eventDate = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if (abs(howRecent) < homeLinerNowValues.timeIntervalOfLocationChangeMonitor.doubleValue)
    {
        _userLocation = location;
        NSLog(@"latitude %+.6f, longitude %+.6f\n", _userLocation.coordinate.latitude, _userLocation.coordinate.longitude);
    }
}

// 測位失敗
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError");
}

// 測位開始
- (void)onResumeLocationManager
{
    // 出来れば大幅変更位置情報サービスを利用したい。
    if (locationManager != nil)
    {
        if ([CLLocationManager significantLocationChangeMonitoringAvailable])
        {
            [locationManager startMonitoringSignificantLocationChanges];
            NSLog(@"Significant location change monitoring is available");
        }
        else if ([CLLocationManager locationServicesEnabled])
        {
            [locationManager startUpdatingLocation];
            NSLog(@"Location services is available");
        }
        _userLocation = locationManager.location;
        NSLog(@"latitude %+.6f, longitude %+.6f\n", _userLocation.coordinate.latitude, _userLocation.coordinate.longitude);
        /*
        if ([CLLocationManager locationServicesEnabled])
        {
            [locationManager startUpdatingLocation];
            NSLog(@"Location services is available");
        }
         */
    }
}

// 測位停止
- (void)onPauseLocationManager
{
    if (locationManager != nil)
    {
        // 出来れば大幅変更位置情報サービスを利用したい。
        if ([CLLocationManager significantLocationChangeMonitoringAvailable])
        {
            [locationManager stopMonitoringSignificantLocationChanges];
            NSLog(@"Significant location change monitoring stoped");
        }
        else if ([CLLocationManager locationServicesEnabled])
        {
            [locationManager stopUpdatingLocation];
            NSLog(@"Location services stoped");
        }
        /*
        if ([CLLocationManager locationServicesEnabled])
        {
            [locationManager stopUpdatingLocation];
            NSLog(@"Location services stoped");
        }
         */
    }
}

// TextField編集開始
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    // ActionSheetを準備
    _delayPickerParent = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil, nil];
    
    // UIDatePickerを作成
    _delayPicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0.0, 44.0, 0.0, 0.0)];
    _delayPicker.datePickerMode = UIDatePickerModeCountDownTimer;

    // Toolbarを作成
    UIToolbar *delayToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 44.0)];
    delayToolbar.barStyle = UIBarStyleBlackOpaque;
    [delayToolbar sizeToFit];
    NSMutableArray *toolbarItems = [[NSMutableArray alloc] init];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(delayPickerDoneClick)];
    [toolbarItems addObject:flexSpace];
    [delayToolbar setItems:toolbarItems animated:YES];

    // pickerとtoolbarを追加
    [_delayPickerParent addSubview:delayToolbar];
    [_delayPickerParent addSubview:_delayPicker];
    [_delayPickerParent showInView:self.view];
    [_delayPickerParent setBounds:CGRectMake(0.0, 0.0, 320.0, 464.0)];
    
    // キーボードの表示を停止する。
    return NO;
}

// 遅延時分入力完了
- (IBAction)delayPickerDoneClick
{
    // pickerから遅延時分を取得してtextfieldに表示する。
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = NSLocalizedString(@"delay", @"delay");
    _delayTextField.text = [dateFormatter stringFromDate:_delayPicker.date];
    // 遅延時分を設定
    _logic.delay = _delayPicker.countDownDuration;
    NSLog(@"_logic.delay: %lf", _logic.delay);
    
    [_delayPickerParent dismissWithClickedButtonIndex:0 animated:YES];
    [_delayTextField resignFirstResponder];
}

@end
