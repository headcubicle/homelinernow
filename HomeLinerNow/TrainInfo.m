//
//  HomeLinerInfo.m
//  HomeLinerNow
//
//  Created by foobar on 2012/12/02.
//
//

#import "TrainInfo.h"

@implementation TrainInfo

- (id)init:(NSString *)trainName trainNumber:(NSString *)trainNumber direction:(BOOL)direction ekinetAvailable:(BOOL)ekinetAvailable timeTable:(NSDictionary *)timeTable
{
    self = [super init];
    
    if (self)
    {
        _trainName = trainName;
        _trainNumber = trainNumber;
        _direction = direction;
        _ekinetAvailable = ekinetAvailable;

        _timeTable = [[NSMutableDictionary alloc] init];
        NSArray *stations = timeTable.allKeys;
        for (NSString *stationName in stations)
        {
            TimeTable *timeTableInfo = [[TimeTable alloc] init:stationName timeTableInfo:[timeTable valueForKey:stationName]];
            [_timeTable setObject:timeTableInfo forKey:stationName];
        }
        
        return self;
    }
    
    return nil;
}

// 全駅の名前を取得
- (NSArray *)allStationNames
{
    return _timeTable.allKeys;
}

// 駅の時刻を取得
- (TimeTable *)timeTableInfoFromKey:(NSString *)key
{
    return [_timeTable valueForKey:key];
}

// 上り
+ (BOOL)inBound
{
    return 0;
}

// 下り
+ (BOOL)outBound
{
    return 1;
}

@end
