//
//  AboardHistory.h
//  HomeLinerNow
//
//  Created by foobar on 2013/01/05.
//
//

#import <Foundation/Foundation.h>

@interface AboardHistory : NSObject <NSCoding>
{
    
}

@property (copy, nonatomic) NSString *trainName;
@property (copy, nonatomic) NSNumber *aboardTime;

@end
